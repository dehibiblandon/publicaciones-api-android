package com.db.publicaciones.Interface;

import com.db.publicaciones.Model.Publicaciones;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ApiService {
    //Complementamos la url del api que se confoguró en el archivo Config
    // ademas se para el parametro
    @GET("posts")
    Call<List<Publicaciones>> getPublicaciones();


}
