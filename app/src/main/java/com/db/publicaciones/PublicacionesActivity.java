package com.db.publicaciones;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import android.widget.ListView;
import android.widget.TextView;

import com.db.publicaciones.Interface.ApiService;
import com.db.publicaciones.Model.Publicaciones;
import com.db.publicaciones.Package.Config;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import androidx.appcompat.app.AppCompatActivity;


public class PublicacionesActivity extends AppCompatActivity {
    private ListView listapost;
    private AdapterPublicaciones adapter;
    private ApiService servicio= Config.getRetrofit().create(ApiService.class);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_publicaciones);
        listapost = findViewById(R.id.lista);
        getPublicaciones();

    }

    private void getPublicaciones(){
        Call<List<Publicaciones>> listCall = servicio.getPublicaciones();
        listCall.enqueue(new Callback<List<Publicaciones>>() {
            @Override
            public void onResponse(Call<List<Publicaciones>> call, Response<List<Publicaciones>> response) {
                if(response.isSuccessful()){
                    Log.d("ERROR",response.body().toString());
                    adapter = new AdapterPublicaciones(getApplicationContext(),response.body());
                    listapost.setAdapter(adapter);
                }
            }
            @Override
            public void onFailure(Call<List<Publicaciones>> call, Throwable t) {
                Log.d("ERROR",t.getMessage());
            }
        });
    }
    class AdapterPublicaciones extends ArrayAdapter<Publicaciones> {
        List<Publicaciones> listapublicacion;
        public AdapterPublicaciones(Context context, List<Publicaciones> lista) {
            super(context, R.layout.item_post, lista);
            listapublicacion = lista;
        }
        @Override
        public View getView(int position,  View convertView, ViewGroup parent) {
            View view= LayoutInflater.from(getContext()).inflate(R.layout.item_post,null);
            //TextView id= view.findViewById(R.id.idPost);
            TextView title= view.findViewById(R.id.titlePost);
            TextView body= view.findViewById(R.id.bodyPost);
            //id.setText(listapublicacion.get(position).getId());
            title.setText(listapublicacion.get(position).getTitle());
            body.setText(listapublicacion.get(position).getBody());
            return view;
        }
    }

}