package com.db.publicaciones.Model;

public class Publicaciones {
    private int id;
    private String  title;
    private String  body;


    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getBody() {
        return body;
    }
}
